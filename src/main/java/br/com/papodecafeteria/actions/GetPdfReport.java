package br.com.papodecafeteria.actions;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import br.com.papodecafeteria.dao.jpa.UserDaoJpa;
import br.com.papodecafeteria.jasperreports.HelloJasperReports;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

public class GetPdfReport extends Action {
	private static Logger l = Logger.getLogger(GetPdfReport.class.getName());
	
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	try{
    		String cLayout = getServlet().getServletContext().getRealPath("/WEB-INF/classes") + "\\helloJasperReport.jrxml";
			
			Map<String, Object> pMap = new HashMap<String, Object>();
			pMap.put("pTitle", "PoC - Papo de Cafeteria");
			pMap.put("pSubTitle", "HelloJasperReports");
			
			pMap.put("pTabFieldName", "Nome");
			pMap.put("pTabFieldEmail", "Email");
			pMap.put("pTabFieldSex", "Sex");
			pMap.put("pTabFieldCountry", "Country");
			
			JasperPrint pJasperPrint = HelloJasperReports.getReport(
					pMap, cLayout, new JRBeanCollectionDataSource(UserDaoJpa.getAllRecords()));
			
			JasperViewer viewer = new JasperViewer(pJasperPrint, true);
			
			viewer.setVisible(true);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
    	
    	return(mapping.findForward("home"));
	}

}
