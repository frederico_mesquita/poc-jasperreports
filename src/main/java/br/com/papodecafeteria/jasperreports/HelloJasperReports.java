package br.com.papodecafeteria.jasperreports;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.papodecafeteria.dao.jpa.UserDaoJpa;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class HelloJasperReports {
	
	private static Logger l = Logger.getLogger(HelloJasperReports.class.getName());
	private static HelloJasperReports instance = null;
	
	private HelloJasperReports(){}
	
	public static HelloJasperReports getInstance(){
		try{
			if(null == instance)
				instance = new HelloJasperReports();
			
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return instance;
	}
	
	public static JasperPrint getReport(Map<String, Object> pMap, String pLayout, JRBeanCollectionDataSource itemsJRBean){
		JasperPrint report = null;
		try {
			report = 
				JasperFillManager.fillReport(
					JasperCompileManager.compileReport( JRXmlLoader.load(pLayout) ), pMap, itemsJRBean );
		} catch (JRException exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return report;
	}
	
	public static void exportReportUsingJasperReport(int pType, String pReportName, Map<String, Object> pMap, String pLayout, JRBeanCollectionDataSource itemsJRBean){
		try {
			JasperPrint jasperPrint = getReport(pMap, pLayout, itemsJRBean);

	        switch (pType) {
	            case 1:  JasperExportManager.exportReportToHtmlFile(jasperPrint, pReportName);
	                     break;
	            case 2:  JasperExportManager.exportReportToXmlFile(jasperPrint, pReportName, false);
	                     break;
	            case 3:  
	            default: JasperExportManager.exportReportToPdfFile(jasperPrint, pReportName);
	                     break;
	        }
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return;
	}
	
	public static enum reportOptionsToExport {
	    HTML(1), XML(2), PDF(3);
	    
	    private final int iValue;
	    reportOptionsToExport(int valorOpcao){
	    	iValue = valorOpcao;
	    }
	    public int getiValue(){
	        return iValue;
	    }
	}

	public static void main(String[] args) {
		try{
			String cLayout = "src/main/resources/helloJasperReport.jrxml";
			
			Map<String, Object> pMap = new HashMap<String, Object>();
			pMap.put("pTitle", "PoC - Papo de Cafeteria");
			pMap.put("pSubTitle", "HelloJasperReports");
			
			pMap.put("pTabFieldName", "Nome");
			pMap.put("pTabFieldEmail", "Email");
			pMap.put("pTabFieldSex", "Sex");
			pMap.put("pTabFieldCountry", "Country");
			
			String cReportResult = "./reports/teste";
			
			exportReportUsingJasperReport(reportOptionsToExport.HTML.getiValue(), cReportResult + ".html", pMap, cLayout, (new JRBeanCollectionDataSource(UserDaoJpa.getAllRecords())));
			exportReportUsingJasperReport(reportOptionsToExport.XML.getiValue(), cReportResult + ".xml", pMap, cLayout, (new JRBeanCollectionDataSource(UserDaoJpa.getAllRecords())));
			exportReportUsingJasperReport(reportOptionsToExport.PDF.getiValue(), cReportResult + ".pdf", pMap, cLayout, new JRBeanCollectionDataSource( UserDaoJpa.getAllRecords()));
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return;
	}

}
