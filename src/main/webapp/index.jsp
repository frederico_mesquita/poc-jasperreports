<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>JasperReports Web Application Example</title>
	</head>
	<body>
		<div align="center" id="home">
			<p>
				<h1>JasperReports Web Application Example</h1>
				<html:link page="/getPdfReport.do">Get JasperReport</html:link>
			</p>
			<p>
			    <address>
			        Written by <a href="mailto:frederico_mesquita@hotmail.com">Frederico Mesquita</a>.<br> 
			        Contact us at: Post Box 210, Belo Horizonte/MG Brazil
			    </address>
			</p>
		</div>
	</body>
</html>